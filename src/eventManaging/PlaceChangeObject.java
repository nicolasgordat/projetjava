/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventManaging;

import java.util.EventObject;
import projetjava.Place;

/**
 * This class is used to represent a changePlace event
 * @author land3r
 */
public class PlaceChangeObject extends EventObject {
 
    private Place value;
    
    /**
     * Constructor with the place and object that triggers the event
     * @param source Object that triggers the event
     * @param place Place in which characters related to the event will be in
     */
    public PlaceChangeObject(Object source, Place place) {
        super(source);
        this.value = place;
    }
 
    /**
     *
     * @return Return the place of the PlaceChangeObject
     */
    public Place getValue() {
        return value;
    }
}
