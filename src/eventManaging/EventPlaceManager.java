/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventManaging;

import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;
import projetjava.Place;


/**
 * This class is used to deal with Event
 * @see EventManagerInterface
 * @author land3r
 */
public class EventPlaceManager implements EventManagerInterface{
    
    private Set<EventPlaceListener> listeners;

    /**
     * Default constructor
     */
    public EventPlaceManager() {
        this.listeners = new HashSet<EventPlaceListener>();
    }

    /**
     * Constructor with the source object that trigger the event
     * @param source Object that triggers the event
     */
    public EventPlaceManager(Object source) {
        this.listeners = new HashSet<EventPlaceListener>();
    }

    @Override
    public void addToListener(EventListener listener) {
        this.listeners.add((EventPlaceListener) listener);
    }

    @Override
    public void removeFromListener(EventListener listener) {
        this.listeners.remove((EventPlaceListener) listener);
    }

    @Override
    public void notifyListeners()
    {
        
    }
    
    /**
     * Notify all listerners registered
     * @param place The place in which characters will now be in
     */
    public void notifyListeners(Place place) {
        for (EventPlaceListener numberReadListener : listeners) {
            // Appeler la fonction associée dans l'objet recevant le signal (SLOT)
            numberReadListener.changePlace(new PlaceChangeObject(this, place));
        }
    }
}
