/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventManaging;

import projetjava.Place;

/**
 * This interface is used to deal with character that send the signal
 * @see EventManagerInterface
 * @author land3r
 */
public interface EventPlaceInterface extends EventManagerInterface{
        
    /**
     * Notify all listeners that a place changement is required
     * @param place
     */
    public void notifyListeners(Place place);

}
