/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventManaging;

import java.util.EventListener;
import projetjava.Place;

/**
 * This interface is used to deal with Event
 * @author land3r
 */
public interface EventManagerInterface {

    /**
     * Add a listener to the list of listening listeners
     * @param listener Listener to add to list of listening listeners
     */
    public void addToListener(EventListener listener);
    /**
     * Remove listener from the list of listening listeners
     * @param listener Listener to remove to list of listening listeners
     */
    public void removeFromListener(EventListener listener);
    /**
     * Alert all listener that the event is send
     */
    public void notifyListeners();
}
