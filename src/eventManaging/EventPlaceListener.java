/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventManaging;

import java.util.EventListener;
/**
 * This interface is used to deal with character that can change place
 * @see EventListener
 * @author land3r
 */
public interface EventPlaceListener extends EventListener{
    
    /**
     * Allow character to change his place
     * @param placeChangeObject
     */
    public void changePlace(PlaceChangeObject placeChangeObject);
}
