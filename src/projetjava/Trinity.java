/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.PlaceChangeObject;
import eventManaging.EventPlaceListener;
import java.util.ArrayList;

/**
 * This class is used to represent character Trinity
 *
 * @see Human
 * @see EventPlaceListener
 * @see Learner
 * @author land3r
 */
public class Trinity extends Human implements EventPlaceListener, Learner {

    private ArrayList<String> skills = new ArrayList<String>();

    /**
     * Default Constructor
     */
    public Trinity() {
        super();
        this.fightSpirit = new FighterSpirit(3);
        but = "Tomber amoureux de l'elu";
    }

    /**
     * Constructor with the name of Trinity
     * @param name Name of Trinity
     */
    public Trinity(String name) {
        super(name);
        this.fightSpirit = new FighterSpirit(3);
        but = "Tomber amoureux de l'elu";
    }

    /**
     * Constructor with the name and Place of Trinity
     * @param name Name of Trinity
     * @param place Place in which Trinity is in
     */
    public Trinity(String name, Place place) {
        super(name, place);
        this.fightSpirit = new FighterSpirit(3);
        but = "Tomber amoureux de l'elu";
    }

    @Override
    public void changePlace(PlaceChangeObject placeChangeObject) {
        boolean wasInRealPlace = this.place.isRealPlace();
        this.place = placeChangeObject.getValue();
        boolean isInRealPlace = this.place.isRealPlace();
        if (!wasInRealPlace && isInRealPlace) {
            OutputManagement.printStoryLn(this.getName().concat(" quitte la matrice et arrive dans ".concat(this.place.getName())));
        } else if (wasInRealPlace && !isInRealPlace) {
            OutputManagement.printStoryLn(this.getName().concat(" rentre dans la matrice et arrive dans ".concat(this.place.getName())));
        } else {
            OutputManagement.printStoryLn(this.getName().concat(" arrive dans ".concat(this.place.getName())));
        }
    }

    @Override
    public void learnSkill(String skill) {
        this.skills.add(skill);
        this.talk("Je connais le ".concat(skill));
    }
}
