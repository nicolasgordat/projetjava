/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a wounded character
 * @see FightSpirit
 * @author land3r
 */
public class WoundedSpirit implements FightSpirit {

    public int fightLevel;

    /**
     * Constructor with the fightLevel
     * @param fightLevel fightLevel of the Character
     */
    public WoundedSpirit(int fightLevel) {
        this.fightLevel = fightLevel;
    }
    
    /**
     * Default Constructor
     */
    public WoundedSpirit() {
        this.fightLevel = 0;
    }
    
    @Override
    public void prepareFight(Character opponent) {
        OutputManagement.printStoryLn(opponent.getName().concat(", je me sens si faible. Je ne pense pas etre capable de me battre"));
    }
    
    @Override
    public int fight(String name, Character opponent) {
        if (0 > opponent.fightSpirit.getFightLevel()) {
            return 1;
        } else if (0 == opponent.fightSpirit.getFightLevel()) {
            OutputManagement.printStoryLn(name.concat(" se protege peniblement des coups de ").concat(opponent.getName()));
            return 0;
        } else {
            OutputManagement.printStoryLn(name.concat(" se fait frappe violament dans l'abdomen par ").concat(opponent.getName()));
            return -1;
        }
    }

    @Override
    public int getFightLevel() {
        return 0;
    }
    
    @Override
    public void upgradeFightLevel()
    {
    }
}
