/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent character Oracle
 * @see Machine
 * @author land3r
 */
public class Oracle extends Machine{
       
    /**
     * Default constructor
     */
    public Oracle()
    {
        super();
        this.fightSpirit = new PeaceSpirit();
        but = "Desequilibrer l'equation";
    }
    
    /**
     * Constructor with the name of the Oracle
     * @param name Name of the Oracle
     */
    public Oracle(String name)
    {
        super(name);
        this.fightSpirit = new PeaceSpirit();
        but = "Desequilibrer l'equation";
    }

    /**
     * Constructor with the name and the Place of the Oracle
     * @param name Name of the Oracle
     * @param place Place in which the Oracle will be in
     */
    public Oracle(String name, Place place) {
        super(name, place);
        this.fightSpirit = new PeaceSpirit();
        but = "Desequilibrer l'equation";
    }
    
    /**
     * Retrieve the goal of a Character
     * @param character Character we wanna retrieve the goal
     * @return Goal of the Character
     */
    public String retrieveBut(Character character)
    {
        return character.but;
    }
}
