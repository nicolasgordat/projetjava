/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a Human character
 * @see Character
 * @author land3r
 */
public class Human extends Character{
     
    /**
     * Default constructor
     */
    public Human() {
        super();
        this.isMachine = false;
    }
    
    /**
     * Constructor with the name of the Human
     * @param name Name of the Human
     */
    public Human(String name) {
        super(name);
        this.isMachine = false;
    }
    
    /**
     * Constructor with the name and the Place in which the Human is in
     * @param name Name of the Human
     * @param place Place in which the Human will be in
     */
    public Human(String name, Place place)
    {
        super(name, place);
        this.isMachine = false;
    }
}
