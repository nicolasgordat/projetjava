/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.EventPlaceManager;

/**
 * This class is used to represent character Epoque
 * @see Human
 * @author land3r
 */
public class Epoque extends Human {
  
    protected EventPlaceManager event;
    
    /**
     * Default Constructor
     */
    public Epoque()
    {
        super();
        this.fightSpirit = new PeaceSpirit(1);
        this.event = new EventPlaceManager();
        but = "Sauver les humains";
    }
    
    /**
     * Constructor with the name of Epoque
     * @param name Name of Epoque
     */
    public Epoque(String name)
    {
        super(name);
        this.fightSpirit = new PeaceSpirit(1);
        this.event = new EventPlaceManager();
        but = "Sauver les humains";
    } 
    
    /**
     * Constructor with the name and Place of Epoque
     * @param name Name of Epoque
     * @param place Place in which Epoque will be
     */
    public Epoque(String name, Place place)
    {
        super(name, place);
        this.fightSpirit = new PeaceSpirit(1);
        this.event = new EventPlaceManager();
        but = "Sauver les humains";
    }    
}
