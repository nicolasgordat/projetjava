/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.PlaceChangeObject;

/**
 * This class is used to represent character Agent
 * @see Machine
 * @author land3r
 */
public class Agent extends Machine{

    /**
     * Default constructor
     */
    public Agent()
    {
        super();
        this.fightSpirit = new FighterSpirit(1);
    }
    
    /**
     * Constructor with name
     * @param name Name of the Agent
     */
    public Agent(String name)
    {
        super(name);
        this.fightSpirit = new FighterSpirit(1);
    }
    
    /**
     * Constructor with name and original place
     * @param name Name of the Agent
     * @param place Place in which the Agent will be in
     */
    public Agent(String name, Place place)
    {
        super(name, place);
        this.fightSpirit = new FighterSpirit(1);
    }
    
    /**
     * Change place
     * @param placeChangeObject Object to change Place
     */
    public void changePlace(PlaceChangeObject placeChangeObject)
    {
        throw new UnsupportedOperationException();
    }
}
