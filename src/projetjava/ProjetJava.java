/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.EventPlaceListener;
import eventManaging.PlaceChangeObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is the main class, telling the storyline
 * @author land3r
 */
public class ProjetJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables
        Map<String, Place> placeRef = new HashMap<String, Place>();
        Map<String, Character> charRef = new HashMap<String, Character>();

        placeRef.put("Neo's flat", new Room("Neo's flat", false));
        placeRef.put("Nightclub", new Room("Nightclub", false));
        placeRef.put("Workplace", new Room("Workplace", false));
        placeRef.put("Strange room", new Room("Strange and little room", false));
        placeRef.put("Starship", new Place("Starship"));
        placeRef.put("Training room", new Room("Training room", false));
        placeRef.put("Oracle's flat", new Room("Oracle's flat", false));
        placeRef.put("Street", new Place("On the street", false));
        placeRef.put("Secured room", new Room("Secured room", false));
        placeRef.put("Machine's city", new Place("Machine's city"));
        placeRef.put("City of Sion", new Place("City of Sion"));

        charRef.put("Unknown_h", new Human("Unknown", placeRef.get("Nightclub")));
        charRef.put("Neo", new Neo("Neo", placeRef.get("Neo's flat")));
        charRef.put("Trinity", new Trinity("Trinity", placeRef.get("Nightclub")));
        charRef.put("Morpheus", new Morpheus("Morpheus", placeRef.get("Strange room")));
        charRef.put("Epoque", new Epoque("Epoque", placeRef.get("Starship")));
        charRef.put("Oracle", new Oracle("Oracle", placeRef.get("Oracle's flat")));
        charRef.put("Unknown_m", new Machine("Unknown", placeRef.get("Machine's city")));
        charRef.put("Smith", new Smith("Smith", placeRef.get("Street")));
        charRef.put("Parent machine", new Machine("Parent Class of All Machines", placeRef.get("Machine's city")));
        charRef.put("Customer", new Customer("Customer", placeRef.get("Neo's flat")));

        // Logic

        OutputManagement.information("Welcome to Matrix");

        OutputManagement.println("Ce programme est une experience interactive basée sur le film Matrix");
        OutputManagement.chapter(1, placeRef.get("Neo's flat"));
        chapter1(charRef, placeRef);

        OutputManagement.chapter(2, placeRef.get("Nightclub"));
        chapter2(charRef, placeRef);

        OutputManagement.chapter(3, placeRef.get("Strange room"));
        chapter3(charRef, placeRef);

        OutputManagement.chapter(4, placeRef.get("Starship"));
        chapter4(charRef, placeRef);
        
        OutputManagement.chapter(5, placeRef.get("Oracle's flat"));
        chapter5(charRef, placeRef);
        /*
         * Respect des consignes
         * Fichiers
         *  Nom du fichier = nom de la classe
         *  Fichier .zip ou .tar.gz
         *  Pas de repertoire pour les projets
         *  Pas que des fichiers java (doc)
         * 
         * Standarts java
         * Nom constantes, variables, methodes, classes
         * 
         * JavaDOC (voir matkauf (javadoc html))
         * Utilisations exeptions (creer propres exeptions)
         * 
         * Interface Utilisateur (ergonomie) /2
         * 
         * Rapport, intro, diagramme de classe /2
         * Sujet, description, travail, difficultés rencontrées /3
         * Sujet, difficultée estimée
         * Etat d'avancement
         * 
         * Classes adaptées, héritage, classes abstraites, interfaces /2
         * Encapsulation adaptée (pas de private avec get et set) /1.5
         * Methodes cohérentes /1.5
         * Codage simple /1.5
         * Factorisation du code, optimisation /1.5
         * 
         * Fonctionnement
         */

        // Il vont voir morpheus

        // Pillule

        // Il passe dans la matrice (change endroit)

        // Il douille

        // Il s'entraine

        // Vs morpheus

        // Il va faire un tour dans matrice (on verra)

        // Il va voir l'oracle

        // Elle lui dit des trucs chelou

        // Apparition de smith (prends controle Humain classique)

        // Il se fight contre lui

        // Win Neo

        // Il leave matrice

        // Morpheus se fait chopper

        // Neo y retourne avec trinity

        // Fight vs Mass Smiths

        // Epic win + retour tous vrai monde

        // Machines pas contentes

        // Commencent a avancer vers ville humaine

        // Neo pas content

        // Il bouge son cul vers ville machine

        // Neo parle aux machines (Smith op, il doit le defoncer, en echange machine stop defoncer ville humaine, accepte)

        // Neo fight Smith original version

        // Epikkkk Win

        // Machines changent ordre et get out de ville humaine

        // Neo die like a boss
    }

    public static void chapter1(Map<String, Character> charRef, Map<String, Place> placeRef) {

        /*
         ((Smith) charRef.get("Smith")).talk(((Neo) charRef.get("Neo")).getName(true));
         ((Smith) charRef.get("Smith")).talk(((Neo) charRef.get("Neo")).getName(false));
         ((Smith) charRef.get("Smith")).talk(charRef.get("Neo").getName());
         */

        ClassicObject hackPrgm = new ClassicObject("Hacking software", charRef.get("Neo"));

        charRef.get("Neo").sleep();
        charRef.get("Unknown_h").talk(charRef.get("Neo").getName().concat(", wake up !"));
        charRef.get("Neo").wakeUp();
        charRef.get("Neo").talk("Who are you ?");
        charRef.get("Unknown_h").talk("It doesn't matter, we've been looking for you as much as you looked for us.");
        charRef.get("Unknown_h").talk(charRef.get("Neo").getName().concat(", they found you ! You must escape !"));
        charRef.get("Neo").talk("Who ? And how ?");
        charRef.get("Unknown_h").talk("Follow the white rabbit.");
        OutputManagement.printStoryLn("");

        ((Room) placeRef.get("Neo's flat")).knockDoor(charRef.get("Customer"));
        charRef.get("Neo").talk("Who's there ?");
        charRef.get("Customer").talk("It's me ".concat(charRef.get("Neo").getName()).concat(", open the door"));
        charRef.get("Neo").talk("Ok, I'm on my way!");
        ((Room) placeRef.get("Neo's flat")).openDoor(charRef.get("Neo"));
        charRef.get("Customer").talk("...");
        charRef.get("Customer").talk(charRef.get("Neo").getName().concat(", have you finished the software ?"));

        charRef.get("Neo").talk("Of course man, It's for you");
        hackPrgm.setProprietary(charRef.get("Customer"));
        OutputManagement.printStoryLn(hackPrgm.getName().concat(" now belongs to ".concat(hackPrgm.getProprietary().getName())));
        charRef.get("Customer").talk(charRef.get("Neo").getName().concat(", do you want to go to the Nightclub with us ?"));
        charRef.get("Neo").talk("Not really, i am kinda tired.");
        ((Neo) charRef.get("Neo")).noticeWhiteRabbit();
        charRef.get("Neo").talk("Ok, I will come");
    }

    public static void chapter2(Map<String, Character> charRef, Map<String, Place> placeRef) {

        PlaceChangeObject place = new PlaceChangeObject(new Object(), placeRef.get("Strange room"));

        charRef.get("Neo").talk("I got to find the girl with that white rabbit.");
        charRef.get("Neo").talk("Where can she be ?");
        charRef.get("Unknown_h").talk(charRef.get("Neo").getName().concat(", I am right here"));
        charRef.get("Neo").talk("Who are you ?");
        charRef.get("Neo").talk("Are you the one whom i chat with this morning ?");
        charRef.get("Unknown_h").talk("Yes, it was me.");
        charRef.get("Unknown_h").talk("My name is Trinity");
        charRef.get("Trinity").talk(charRef.get("Neo").getName().concat(", you got to trust me."));
        charRef.get("Trinity").talk(charRef.get("Neo").getName().concat(", you know the world isn't going round but you don't know why."));
        charRef.get("Trinity").talk("I can help you find what you want.");
        charRef.get("Neo").talk("And what do I want ?");
        charRef.get("Trinity").talk("You want answers, and i can lead you to the guy that know thoses answers.");
        charRef.get("Neo").talk("Morpheus ?");
        charRef.get("Trinity").talk("Yes Morpheus.");

        OutputManagement.println("");
        ((Neo) charRef.get("Neo")).changePlace(place);
        ((Trinity) charRef.get("Trinity")).changePlace(place);
        OutputManagement.println("");


        // Partie DEMO fonctionnement events Epoque

        /*
         * ((Epoque) charRef.get("Epoque")).event.addToListener((EventPlaceListener) charRef.get("Neo"));
         //eventManagerInterface.removeFromListener((EventPlaceListener) charRef.get("Neo"));

         OutputManagement.printStoryLn("Valeur Neo : ".concat(charRef.get("Neo").place.getName()));

         ((Epoque) charRef.get("Epoque")).event.notifyListeners(placeRef.get("Oracle's flat"));

         OutputManagement.printStoryLn("Valeur Neo : ".concat(charRef.get("Neo").place.getName()));
         */
    }

    public static void chapter3(Map<String, Character> charRef, Map<String, Place> placeRef) {

        String[] str = {"Blue pill", "Red pill"};
        int result = OutputManagement.question("Would you take the red pill or the blue pill ?", str);
        if (result == 0) {
            charRef.get("Morpheus").talk("Ok, c'est ton choix ".concat(charRef.get("Neo").getName()));
            charRef.get("Morpheus").talk("Ton aventure s'arrete donc ici.");
            System.exit(0);
        } else {
            charRef.get("Morpheus").talk("Bon choix");
        }


        // Partie changement, go dans le monde reel
        ((Epoque) charRef.get("Epoque")).event.addToListener((EventPlaceListener) charRef.get("Neo"));
        ((Epoque) charRef.get("Epoque")).event.addToListener((EventPlaceListener) charRef.get("Morpheus"));
        ((Epoque) charRef.get("Epoque")).event.addToListener((EventPlaceListener) charRef.get("Trinity"));
        ((Epoque) charRef.get("Epoque")).event.notifyListeners(placeRef.get("Starship"));
    }

    public static void chapter4(Map<String, Character> charRef, Map<String, Place> placeRef) {
        ArrayList<String> floppyDisk = new ArrayList<String>();
        floppyDisk.add("Kungfu");
        floppyDisk.add("Free fight");
        floppyDisk.add("Jujitsu");
        floppyDisk.add("Krav maga");
        floppyDisk.add("Karate");
        floppyDisk.add("Judo");
        floppyDisk.add("Taekwondo");
        floppyDisk.add("Tennis");
        floppyDisk.add("Ping pong");
        floppyDisk.add("JAVA I");
        floppyDisk.add("Binaire");
        floppyDisk.add("Saut de batiment en batiments");

        //Regarder si on peut passer une collection en parametre, pour eviter d'avoir tout ces .add
        for (String skill : floppyDisk) {
            ((Neo) charRef.get("Neo")).learnSkill(skill);
        }

        // Fight avec Morpheus
        ArrayList<Character> team1 = new ArrayList<Character>(), team2 = new ArrayList<Character>();
        team1.add(charRef.get("Neo"));
        team2.add(charRef.get("Morpheus"));
        fight(team1, team2, 1);

        // Fight avec Smith apres oracle
        team2 = new ArrayList<Character>();
        team1 = new ArrayList<Character>();
        team1.add(charRef.get("Morpheus"));
        team1.add(charRef.get("Neo"));
        team2.add(charRef.get("Smith"));
        fight(team1, team2, 2);

        // Fight sauver morpheus
        ((Morpheus) charRef.get("Morpheus")).setWoundedSpiritFighter();
        team2 = new ArrayList<Character>();
        team1 = new ArrayList<Character>();
        team1.add(charRef.get("Morpheus"));
        team1.add(charRef.get("Neo"));
        team1.add(charRef.get("Trinity"));
        team2.add(charRef.get("Smith"));
        team2.add(charRef.get("Smith"));
        team2.add(charRef.get("Smith"));
        fight(team1, team2, 3);

        // Fight finale
        ((Morpheus) charRef.get("Morpheus")).setFightSpiritFighter();
        team2 = new ArrayList<Character>();
        team1 = new ArrayList<Character>();
        team1.add(charRef.get("Neo"));
        team2.add(charRef.get("Smith"));
        fight(team1, team2, 4);
    }

    public static void chapter5(Map<String, Character> charRef, Map<String, Place> placeRef) {
        
        String goal;
        goal = ((Oracle) charRef.get("Oracle")).retrieveBut(charRef.get("Neo"));
        charRef.get("Oracle").talk(charRef.get("Neo").getName().concat(", ton but est de ").concat(goal));
    }

    public static void fight(ArrayList<Character> team1, ArrayList<Character> team2, int fightCount) {
        int winCount = 0;
        if (fightCount == 4 || fightCount == 3) {
            winCount = 1;
        }

        OutputManagement.println("Let's fight !");
        OutputManagement.println("-------------------");
        String team1String = "", team2String = "";
        for (Character fighter : team1) {
            OutputManagement.print(fighter.getName().concat(" : "));
            fighter.fightSpirit.prepareFight(team2.get(0));
            if ("".equals(team1String)) {
                team1String = team1String.concat(fighter.getName());
            } else {
                team1String = team1String.concat(", ".concat(fighter.getName()));
            }
        }
        for (Character fighter : team2) {
            OutputManagement.print(fighter.getName().concat(" : "));
            fighter.fightSpirit.prepareFight(team1.get(0));
            if ("".equals(team2String)) {
                team2String = team2String.concat(fighter.getName());
            } else {
                team2String = team2String.concat(", ".concat(fighter.getName()));
            }
        }
        OutputManagement.println("-------------------");
        OutputManagement.printStoryLn(team1String.concat(" VS ").concat(team2String));
        OutputManagement.println("-------------------");

        for (Character fighter1 : team1) {
            for (Character fighter2 : team2) {
                int result = fighter1.fightSpirit.fight(fighter1.getName(), fighter2);
                fighter2.fightSpirit.fight(fighter2.getName(), fighter1);
                if (fightCount == 4) {
                    winCount++;
                }
                if (result == 1) {
                    // Win fighter1
                    fighter2.fightSpirit.upgradeFightLevel();
                    winCount++;
                } else if (result == 0) {
                    // Egalité
                    fighter1.fightSpirit.upgradeFightLevel();
                } else {
                    // Win fighter 2
                    fighter1.fightSpirit.upgradeFightLevel();
                    winCount--;
                }
            }
        }

        if (winCount > 0) {
            if (team1.size() == 1) {
                OutputManagement.printStoryLn(team1String.concat(" a gagne !"));
            } else {
                OutputManagement.printStoryLn(team1String.concat(" ont gagne !"));
            }
        } else if (winCount < 0) {
            if (team2.size() == 1) {
                OutputManagement.printStoryLn(team2String.concat(" a gagne !"));
            } else {
                OutputManagement.printStoryLn(team2String.concat(" ont gagne !"));
            }
        } else {
            OutputManagement.printStoryLn(team1String.concat(", ").concat(team2String).concat(" sont tous repartis battu, et epuise"));
        }

        winCount = 0;
    }
}
