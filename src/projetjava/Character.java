/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This abstract class is used as the primary block for character
 * @author land3r
 */
abstract public class Character {

    public String name;
    protected FightSpirit fightSpirit;
    protected Place place;
    public String but;
    
    protected boolean isMachine;
    
    public boolean isSleeping;

    /**
     * Default Constructor
     */
    public Character() {
    }

    /**
     * Constructor with name
     * @param name Name of the Character
     */
    public Character(String name) {
        this.setName(name);
    }
    
    /**
     * Constructor with name and place in which Character will be
     * @param name Name of the Character
     * @param place Place in which the Character will be in
     */
    public Character(String name, Place place)
    {
        this.setName(name);
        this.setPlace(place);
    }

    /**
     * Set name of the character
     * @param value Name of the character
     * @return true if name has been correctly set
     */
    public final boolean setName(String value) {
        this.name = value;
        return true;
    }
    
    /**
     * Set the Place of the Character
     * @param place Place in which the Character will be in
     * @return true if Place has been correctly 
     */
    public final boolean setPlace(Place place)
    {
        this.place = place;
        return true;
    }

    /**
     * Return the name of the Character
     * @return Name of the Character
     */
    public String getName() {
        return this.name;
    }

    /**
     * Make the Character sleep
     */
    public void sleep() {
        this.isSleeping = true;
        this.talk("zzzZZZzzz");
    }

    /**
     * Make the Character wake up
     */
    public void wakeUp() {
        this.isSleeping = false;
    }

    /**
     * Return if the Character is sleeping or not
     * @return true if the Character is sleeping
     */
    public boolean isSleeping() {
        return this.isSleeping;
    }

    /**
     * Make the Character talk
     * @param talk What the Character is gonna say
     */
    public void talk(String talk) {
        OutputManagement.print(this.getName().concat(" : "));
        OutputManagement.printStoryLn(talk);
    }
    
    /**
     * Make the Character think
     * @param though What the Character is gonna think
     */
    public void think (String though) {
        OutputManagement.print(this.getName().concat(" ("));
        OutputManagement.printStoryLn(though);
        OutputManagement.printStoryLn(" )");
    }
    
    /**
     * Return if the Character is a Machine
     * @return true if the Character is a Machine
     */
    public boolean isMachine()
    {
        return this.isMachine;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}
