/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a fighter spirit
 * @see FightSpirit
 * @author land3r
 */
public class FighterSpirit implements FightSpirit {

    public int fightLevel;

    /**
     * Default constructor
     */
    public FighterSpirit() {
        this.fightLevel = 0;
    }
    
    /**
     * Constructor with fightLevel
     * @param fightLevel fightLevel of the Character
     */
    public FighterSpirit(int fightLevel) {
        this.fightLevel = fightLevel;
    }


    @Override
    public void prepareFight(Character opponent) {
        OutputManagement.printStoryLn(opponent.getName().concat(", je vais te botter les fesses !"));
    }

    @Override
    public int fight(String name, Character opponent) {
        if (this.getFightLevel() > opponent.fightSpirit.getFightLevel()) {
            OutputManagement.printStoryLn(name.concat(" assene un gros coup a ").concat(opponent.getName()));
            return 1;
        } else if (this.getFightLevel() == opponent.fightSpirit.getFightLevel()) {
            OutputManagement.printStoryLn(name.concat(" se protege efficacement des coups de ").concat(opponent.getName()));
            return 0;
        } else {
            OutputManagement.printStoryLn(name.concat(" se protege peniblement des coups de ").concat(opponent.getName()));
            return -1;
        }
    }

    @Override
    public int getFightLevel() {
        return this.fightLevel;
    }

    @Override
    public void upgradeFightLevel() {
        this.fightLevel++;
    }
}
