/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.EventPlaceListener;
import eventManaging.PlaceChangeObject;

/**
 * This class is used to represent character Trinity
 * @see Human
 * @see EventPlaceListener
 * @author land3r
 */
public class Morpheus extends Human implements EventPlaceListener{
    
    /**
     * Default Constructor
     */
    public Morpheus()
    {
        super();
        this.fightSpirit = new FighterSpirit(2);
        but = "Trouver l'elu";
    }
    
    /**
     * Constructor with the name of Morpheus
     * @param name Name of Morpheus
     */
    public Morpheus(String name)
    {
        super(name);
        this.fightSpirit = new FighterSpirit(2);
        but = "Trouver l'elu";
    }

    /**
     * Constructor with the name and Place of Morpheus
     * @param name Name of Morpheus
     * @param place Place in which Morpheus will be in
     */
    public Morpheus(String name, Place place) {
        super(name, place);
        this.fightSpirit = new FighterSpirit(2);
        but = "Trouver l'elu";
    }
    
    /**
     * Change the fightSpirit into a WoundedSpirit
     * @see WoundedSpirit
     */
    public void setWoundedSpiritFighter()
    {
        this.fightSpirit = new WoundedSpirit(this.fightSpirit.getFightLevel());
    }
    
    /**
     * Change the fightSpirit into a FighterSpirit
     * @see FighterSpirit
     */
    public void setFightSpiritFighter()
    {
        this.fightSpirit = new FighterSpirit(this.fightSpirit.getFightLevel());
    }        
    @Override
    public void changePlace(PlaceChangeObject placeChangeObject)
    {
        boolean wasInRealPlace = this.place.isRealPlace();
        this.place = placeChangeObject.getValue();
        boolean isInRealPlace = this.place.isRealPlace();
        if (!wasInRealPlace && isInRealPlace) OutputManagement.printStoryLn(this.getName().concat(" quitte la matrice et arrive dans ".concat(this.place.getName())));
        else if(wasInRealPlace && !isInRealPlace) OutputManagement.printStoryLn(this.getName().concat(" rentre dans la matrice et arrive dans ".concat(this.place.getName())));
        else OutputManagement.printStoryLn(this.getName().concat(" arrive dans ".concat(this.place.getName())));
    }
}
