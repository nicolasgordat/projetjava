/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import javax.swing.*;

/**
 * This class is used to deal with all Output
 * @author land3r
 */
public class OutputManagement {

    public static int sleepTime = 1;
    private static String prgmName = "Matrix";

    /**
     * Default Constructor
     */
    public OutputManagement() {
    }

    /**
     * Print in Story mode
     * @param story Text to be print
     * @param sleepTime Time to wait between all characters in ms
     */
    public static void printStory(String story, int sleepTime) {
        char c;
        for (int i = 0; i < story.length(); i++) {
            c = story.charAt(i);
            System.out.write(c);
            sleep(sleepTime);
        }
    }

    /**
     * Print in Story mode
     * @param story Text to be print
     */
    public static void printStory(String story) {
        char c;
        for (int i = 0; i < story.length(); i++) {
            c = story.charAt(i);
            System.out.write(c);
            sleep(OutputManagement.sleepTime);
        }
    }

    /**
     * Println in Story mode
     * @param story Text to be println
     * @param sleepTime Time to wait between all characters in ms
     */
    public static void printStoryLn(String story, int sleepTime) {
        char c;
        for (int i = 0; i < story.length(); i++) {
            c = story.charAt(i);
            System.out.write(c);
            sleep(sleepTime);
        }
        System.out.print("\n");
    }

    /**
     * Println in Story mode
     * @param story Text to be println
     */
    public static void printStoryLn(String story) {
        char c;
        for (int i = 0; i < story.length(); i++) {
            c = story.charAt(i);
            System.out.write(c);
            sleep(OutputManagement.sleepTime);
        }
        System.out.print("\n");
    }

    /**
     * Print text
     * @param story Text to be print
     */
    public static void print(String story) {
        System.out.print(story);
    }

    /**
     * Println text
     * @param story Text to be print
     */
    public static void println(String story) {
        System.out.println(story);
    }

    private static void sleep(int time) {
        long startTime = System.currentTimeMillis();
        long endTime = startTime;

        while (endTime - startTime < time) {
            endTime = System.currentTimeMillis();
        }
    }

    /**
     * Print presentation for a chapter
     * @param i Index of the chapter
     */
    public static void chapter(int i) {
        System.out.println();
        System.out.println("                                Chapitre ".concat(String.valueOf(i)));
        System.out.println("                           -------------------");
        System.out.println();

    }
    
    /**
     * Print presentation for a chapter
     * @param i index of the chapter
     * @param place Place in which the chapter take place
     */
    public static void chapter(int i, Place place) {
        System.out.println();
        System.out.println("                                Chapitre ".concat(String.valueOf(i)));
        System.out.println("                           -------------------");
        System.out.println("                                Dans ".concat(place.getName()));
        System.out.println();
    }
    
    /**
     * Print an information
     * @param string Text to be shown
     */
    public static void information(String string) {
        JOptionPane.showMessageDialog(null, string, prgmName, JOptionPane.INFORMATION_MESSAGE);
    }
    
    /**
     * Ask a question
     * @param question Question to be shown
     * @param values Differents values to be proposed as an answer
     * @return Index of the chosen answer
     */
    public static int question(String question, String[] values)
    {
        JOptionPane j = new JOptionPane();
        return JOptionPane.showOptionDialog(null, question, prgmName, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, values, values[0]);
    }
    
}
