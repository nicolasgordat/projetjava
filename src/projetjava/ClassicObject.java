/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a simple Object
 * @author land3r
 */
public class ClassicObject {
    
    private String name;
    private Character proprietary;

    /**
     * Default Constructor
     */
    public ClassicObject() {
        this.name = "";
        this.proprietary = new Human();
    }
    
    /**
     * Constructor with the proprietary
     * @param proprietary Character that possess the ClassicObject
     */
    public ClassicObject(Character proprietary) {
        this.name = "";
        this.proprietary = proprietary;
    }
    
    /**
     * Constructor with the proprietary and name of the ClassicObject
     * @param name The name of the ClassicObject
     * @param proprietary Character that possess the ClassicObject
     */
    public ClassicObject(String name, Character proprietary) {
        this.name = name;
        this.proprietary = proprietary;
    }
    
    /**
     * Set the proprietary of the ClassicObject
     * @param proprietary Character that will possess the ClassicObject
     * @return true if the Proprietary was correctly set
     */
    public boolean setProprietary(Character proprietary) {
        this.proprietary = proprietary;
        return true;
    }
    
    /**
     * Return the proprietary of the ClassicObject
     * @return The proprietary of the ClassicObject
     */
    public Character getProprietary() {
        return this.proprietary;
    }
    
    /**
     * Return the name of the ClassicObject
     * @return The name of the ClassicObject
     */
    public String getName()
    {
        return this.name;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}
