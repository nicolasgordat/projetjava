/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent the peace spirit
 * @see FightSpirit
 * @see Character
 * @author land3r
 */
public class PeaceSpirit implements FightSpirit{
    
    public int fightLevel;
    
    /**
     * Default Constructor
     */
    public PeaceSpirit()
    {
        this.fightLevel = 0;
    }
    
    /**
     * Constructor with the fightLevel
     * @param fightLevel
     */
    public PeaceSpirit(int fightLevel) {
        this.fightLevel = fightLevel;
    }
    
    @Override
    public void prepareFight(Character opponent)
    {
        OutputManagement.printStoryLn(opponent.getName().concat(", je n'ai pas envie de me battre, si cela doit etre ma fin, je prefere l'accepter."));
    }
        
    @Override
    public int fight(String name, Character opponent) {
        return -1;
    }

    @Override
    public int getFightLevel() {
        return this.fightLevel;
    }
    
    @Override
    public void upgradeFightLevel()
    {
        this.fightLevel++;
    }
}
