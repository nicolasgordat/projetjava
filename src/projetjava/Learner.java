/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This interface is used to represent the ability of learning
 * @see Character
 * @author land3r
 */
public interface Learner {
    
    /**
     * Learn the skill
     * @param skill Name of the skill
     */
    public void learnSkill(String skill);

}
