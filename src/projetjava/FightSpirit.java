/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This interface is used to represent a character combativity
 * @see Character
 * @author land3r
 */
public interface FightSpirit {
    
    /**
     * Prepare Character to fight
     * @param opponent Character to fight
     */
    public void prepareFight(Character opponent);
    
    /**
     * Make Character fight
     * @param name Name of the Character
     * @param opponent Character to fight
     * @return 1 if the Character won<br />0 if it's a draw<br />-1 if the Character lose
     */
    public int fight(String name, Character opponent);
    
    /**
     * Return the fightLevel of the Character
     * @return The fightLevel of the Character
     */
    public int getFightLevel();

    /**
     * Upgrade Character's fightLevel
     */
    public void upgradeFightLevel();
}
