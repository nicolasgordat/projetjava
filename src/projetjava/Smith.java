/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.PlaceChangeObject;
import eventManaging.EventPlaceListener;

/**
 * This class is used to represent character Smith Agent
 * @see Agent
 * @author land3r
 */
public class Smith extends Agent implements EventPlaceListener{
               
    /**
     * Default Constructor
     */
    public Smith()
    {
        super();
        this.fightSpirit.upgradeFightLevel();
        but = "Detruire l'elu";
    }
    
    /**
     * Constructor with the name of Smith
     * @param name Name of Smith
     */
    public Smith(String name)
    {
        super(name);
        this.fightSpirit.upgradeFightLevel();
        but = "Detruire l'elu";
    }

    /**
     * Constructor with the name and Place of Smith
     * @param name Name of Smith
     * @param place Place in which Smith will be in
     */
    public Smith(String name, Place place) {
        super(name, place);
        this.fightSpirit.upgradeFightLevel();
        but = "Detruire l'elu";
    }
    
    @Override
    public void changePlace(PlaceChangeObject placeChangeObject)
    {
        boolean wasInRealPlace = this.place.isRealPlace();
        this.place = placeChangeObject.getValue();
        boolean isInRealPlace = this.place.isRealPlace();
        if (!wasInRealPlace && isInRealPlace) OutputManagement.printStoryLn(this.getName().concat(" quitte la matrice et arrive dans ".concat(this.place.getName())));
        else if(wasInRealPlace && !isInRealPlace) OutputManagement.printStoryLn(this.getName().concat(" rentre dans la matrice et arrive dans ".concat(this.place.getName())));
        else OutputManagement.printStoryLn(this.getName().concat(" arrive dans ".concat(this.place.getName())));
    }
}
