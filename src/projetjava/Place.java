/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a Place
 * @author land3r
 */
public class Place {

    public String name;
    private boolean isRealPlace = true;

    /**
     * Default Constructor
     */
    public Place() {
        setName("");
        isRealPlace = true;
    }

    /**
     * Constructor with the name of the Place
     * @param name Name of the Place
     */
    public Place(String name) {
        this.setName(name);
    }

    /**
     * Constructor with the name and categorie of the Place
     * @param name Name of the Place
     * @param truePlace Is it a true Place
     */
    public Place(String name, boolean truePlace) {
        this.setName(name);
        this.isRealPlace = truePlace;
    }

    /**
     * Set the name of the Place
     * @param name Name of the Place
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieve the name of the Place
     * @return Name of the Place
     */
    public String getName() {
        return this.name;
    }

    /**
     * Return if the Place is real place
     * @return true if it is a real Place
     */
    public boolean isRealPlace() {
        return this.isRealPlace;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}
