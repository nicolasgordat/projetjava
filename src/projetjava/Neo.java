/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import eventManaging.EventPlaceListener;
import eventManaging.PlaceChangeObject;
import java.util.ArrayList;

/**
 * This class is used to represent character Neo
 * @see Human
 * @see EventPlaceListener
 * @see Learner
 * @author land3r
 */
public class Neo extends Human implements EventPlaceListener,Learner{
    
    private ArrayList<String> skills = new ArrayList<String>();
    
    /**
     * Default Constructor
     */
    public Neo() {
        super();
        this.fightSpirit = new FighterSpirit(1);
        but = "Sauver l'humanite";
    }

    /**
     * Constructor with the name of Neo
     * @param name Name of Neo
     */
    public Neo(String name) {
        super(name);
        this.fightSpirit = new FighterSpirit(1);
        but = "Sauver l'humanite";
    }

    /**
     * Constructor with the name and Place of Neo
     * @param name Name of Neo
     * @param place Place in which Neo will be in
     */
    public Neo(String name, Place place) {
        super(name, place);
        this.fightSpirit = new FighterSpirit(1);
        but = "Sauver l'humanite";
    }

    /**
     * Return the name of Neo
     * @param Smith Is it a Smith Agent calling it ?
     * @return Name of Neo
     */
    final public String getName(boolean Smith)
    {
        if(Smith)
        {
            return "Mr. Anderson";
        }
        else return super.getName();
    }
    
    /**
     * Make Neo notice the White Rabbit
     */
    public void noticeWhiteRabbit()
    {
        OutputManagement.printStoryLn(this.getName().concat(" (Oh mon dieu, elle a un tatouage de lapin blanc sur l'epaule...)"));
    }
    
    @Override
    public void changePlace(PlaceChangeObject placeChangeObject)
    {
        boolean wasInRealPlace = this.place.isRealPlace();
        this.place = placeChangeObject.getValue();
        boolean isInRealPlace = this.place.isRealPlace();
        if (!wasInRealPlace && isInRealPlace) {
            OutputManagement.printStoryLn(this.getName().concat(" quitte la matrice et arrive dans ".concat(this.place.getName())));
            OutputManagement.information(this.getName().concat(" quitte la matrice."));
        }
        else if(wasInRealPlace && !isInRealPlace) {
            OutputManagement.printStoryLn(this.getName().concat(" rentre dans la matrice et arrive dans ".concat(this.place.getName())));
            OutputManagement.information(this.getName().concat(" arrive dans la matrice."));
        }
        else OutputManagement.printStoryLn(this.getName().concat(" arrive dans ".concat(this.place.getName())));
    }

    @Override
    public void learnSkill(String skill)
    {
        this.skills.add(skill);
        this.talk("Je connais le ".concat(skill));
    }
}
