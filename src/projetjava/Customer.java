/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent character Customer
 * @see Human
 * @author land3r
 */
public class Customer extends Human{
      
    /**
     * Default Constructor
     */
    public Customer()
    {
        super();
        this.fightSpirit = new PeaceSpirit();
    }
    
    /**
     * Constructor with the name of the Customer
     * @param name Name the Customer
     */
    public Customer(String name)
    {
        super(name);
        this.fightSpirit = new PeaceSpirit();
    }

    /**
     * Constructor with the name and the Place of the Customer
     * @param name Name of the Customer
     * @param place Place in which the Customer will be in
     */
    public Customer(String name, Place place) {
        super(name, place);
        this.fightSpirit = new PeaceSpirit();
    }
}
