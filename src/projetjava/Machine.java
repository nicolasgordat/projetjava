/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a machine characters
 * @see Character
 * @author land3r
 */
public class Machine extends Character {

    /**
     * Default Constructor
     */
    public Machine() {
        super();
        this.isMachine = true;
    }

    /**
     * Constructor with the name of the Machine
     * @param name Name of the Machine
     */
    public Machine(String name) {
        super(name);
        this.isMachine = true;
    }
    
    /**
     * Constructor with the name and Place of the Machine
     * @param name Name of the Machine
     * @param place Place in which the Machine will be in
     */
    public Machine(String name, Place place)
    {
        super(name, place);
        this.isMachine = true;
    }
}
