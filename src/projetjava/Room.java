/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 * This class is used to represent a Room
 * @see Place
 * @author land3r
 */
public class Room extends Place{
    
    /**
     * Default constructor
     */
    public Room()
    {
        super();
    }
    
    /**
     * Constructor with name of the Room
     * @param name Name of the Room
     */
    public Room(String name)
    {
        super(name);
    }
    
    /**
     * Constructor with name and categorie of the Room
     * @param name Name of the Room
     * @param virtual Is it a real place
     */
    public Room(String name, boolean virtual)
    {
        super(name, virtual);
    }
    
    /**
     * Knock at the door of the Room
     * @param knocker Character that knock at the door of the Room
     */
    public void knockDoor(Character knocker)
    {
        OutputManagement.printStoryLn(knocker.getName().concat(" tape a la porte."));
    }
    
    /**
     * Open the door of the Room
     * @param opener Character that open the door of the Room
     */
    public void openDoor(Character opener)
    {
        OutputManagement.printStoryLn(opener.getName().concat(" ouvre la porte de ").concat(this.getName()));
    }
}
